**Présentation du projet**

CV en ligne de Bradley Cooper.

**Caractéristiques du CV**
Adaptable
Statique
Lien vers un formulaire

**Outils utilisés**
Font awesome pour les icônes

**Charte graphique**
jaune rgb(255, 158, 1)
bleu rgb(30, 139, 196)
vert rgb(53, 188, 122)
rouge rgb(241, 90, 73)
orange rgb(248, 105, 37)